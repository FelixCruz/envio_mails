from crud.views import views as user_views
from django.urls import path

urlpatterns = [
    path(
        route='usuario',
        view=user_views.Userview.as_view(),
        name='crear_usuario'
    ),
    path(
        route='user_nuevo',
        view=user_views.Modificar_user.as_view(),
        name='nuevo_user'
    ),
    path(
        route='mail',
        view=user_views.Email_view.as_view(),
        name='enviar mail'
    ),
    path(
       route='comentarios',
       view=user_views.Form_comentario.as_view(),
       name='enviar_comentarios' 
    )
   
]
