from rest_framework import serializers
from crud.models.user import User
from crud.models.cities import City
from crud.models.emails import Mails
from django.contrib.auth import password_validation, authenticate

class UserSerializers(serializers.Serializer):

    first_name = serializers.CharField(required=False)
    last_name = serializers.CharField(required=False)
    email = serializers.CharField(required=False)
    password = serializers.CharField(required=False)
    mobil_phone = serializers.CharField(required=False)
    profile_id = serializers.IntegerField(required=False)
    #en el modelo es city pero en la base de datos el campo se llama city_id
    city_id = serializers.IntegerField(required=False)

    def create(self,data):
        profile_id = data['profile_id']
        del data['profile_id']
        user = User.objects.create(**data)
        #en el modelo user hay un campo con relacion muchos a muchos llamado profile
        user.profile.add(profile_id)
        # user.set_password(data['password'])
        user.save()
        return user

# class Email_serializado(serializers.ModelSerializer):
#     class Meta:
#         model = Users
#         # fields = ['id','first_name','last_name','nids','gender','profile_id','password','mobil_phone']
#         #exclude = ['edad']
#         fields = '__all__'
#         depth = 1


class Email_serializer(serializers.Serializer):
     
     asunto = serializers.CharField(required=False)
     mensaje = serializers.CharField(required=False)
     email_envio = serializers.CharField(required=False)

     def create(self,data):
         mail = Mails.objects.create(**data)
         mail.save()
         return mail





