# from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from crud.serializers.serializers import UserSerializers,Email_serializer
from crud.models.user import User
from django.core.mail import send_mail
from django.conf import settings
from django.shortcuts import render,redirect
from rest_framework.parsers import JSONParser

class Userview(APIView):


    def post(self,request):
        serializador = UserSerializers(data=request.data)
        serializador.is_valid()
        serializador.save()
        return Response({},status=201)

class Email_view(APIView):

    def post(self,request):
        asunto=request.data['asunto']
        email=request.data['email_envio']
        mensaje=request.data['mensaje'] + " " + email
        serializador=Email_serializer(data=request.data)
        serializador.is_valid()
        serializador.save()
        email_from=settings.EMAIL_HOST_USER
        recipient_list=[email]
        send_mail(asunto,mensaje,email_from,recipient_list)
        
        return Response({},status=201)

        # return Response(serializador.data,status=201)

class Form_comentario(APIView):
    
    def get(self,request):

        return render(request,"mails.html")

class Modificar_user(APIView):

    def post(self,request):
        nombre="miNombre "+":"+request.data['first_name']
        apellido=request.data['last_name']
        email=request.data['email']
        password=request.data['password']
        city_id=request.data['city_id']+1
        profile_id=request.data['profile_id']
        datos={"first_name":nombre,'last_name':apellido,'email':email,'password':password,'city_id':city_id,'profile_id':profile_id}
        
        serializador=UserSerializers(data=datos)
        serializador.is_valid()
        serializador.save()
        return Response({},status=201)




