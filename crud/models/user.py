from django.db import models
from django.core.validators import MinLengthValidator
from crud.models.profileType import ProfileType
from crud.models.cities import City


class User(models.Model):
    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=20)
    email = models.CharField(max_length=30, unique=True)
    password = models.TextField(max_length=50, validators=[
        MinLengthValidator(11)])
    mobil_phone = models.CharField(max_length=11)
    profile = models.ManyToManyField(ProfileType, db_table='users_profile')
    city = models.ForeignKey(City, on_delete=models.CASCADE, null=True, blank=True)

    class Meta:
        db_table = 'user'
