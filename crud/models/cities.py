from django.db import models


class City(models.Model):
    name = models.CharField(max_length=30)
    code = models.CharField(max_length=20)
    latitud = models.FloatField()
    longitud = models.FloatField()
    activate = models.BooleanField(default=True)

    class Meta:
        db_table = 'cities'
