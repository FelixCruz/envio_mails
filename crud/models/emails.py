from django.db import models

class Mails(models.Model):
    email_envio = models.CharField(max_length=30)
    asunto = models.CharField(max_length=30)
    mensaje = models.TextField(max_length=80)

    class Meta:
        db_table = 'comentarios'
    